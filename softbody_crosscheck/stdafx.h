// === Copyright (c) 2020-2021 easimer.net. All rights reserved. ===
//
// Purpose: precompiled header
//

#pragma once

#define GLM_ENABLE_EXPERIMENTAL

#include <cassert>
#include <cstdio>

#include <memory>
#include <functional>
#include <optional>
#include <vector>
#include <unordered_map>
#include <list>
#include <map>
