// === Copyright (c) 2020-2021 easimer.net. All rights reserved. ===
//
// Purpose: Catch2 entry point
//

#include "stdafx.h"
#define CATCH_CONFIG_MAIN
#include <catch2/catch.hpp>
