// === Copyright (c) 2020-2021 easimer.net. All rights reserved. ===
//
// Purpose: objscan pch
//

#pragma once

#include <cstdio>
#include <fstream>
#include <vector>
#include <thread>
#include <mutex>
#include <queue>
#include <objscan.h>
#include "tiny_obj_loader.h"

#include <glm/glm.hpp>
#include <glm/gtc/constants.hpp>