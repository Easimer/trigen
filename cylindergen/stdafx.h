// === Copyright (c) 2020-2021 easimer.net. All rights reserved. ===
//
// Purpose: precompiled headers
//

#pragma once

#define _USE_MATH_DEFINES
#include <cstdio>
#include <cassert>
#include <ctime>
#include <cmath>

#include <functional>
#include <future>
#include <memory>
#include <optional>
#include <variant>
#include <fstream>
#include <sstream>
#include <stack>
#include <vector>
#include <string>
#include <unordered_map>
#include <queue>

#include <SDL.h>
