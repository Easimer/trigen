// === Copyright (c) 2020-2021 easimer.net. All rights reserved. ===
//
// Purpose: General data structures
//

#pragma once
#include <trigen/linear_math.h>
#include <cstdint>
#include <cassert>
#include <vector>

