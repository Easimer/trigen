// === Copyright (c) 2020-2021 easimer.net. All rights reserved. ===
//
// Purpose: entry point
//

#include "stdafx.h"
#include "application.h"

int main(int argc, char** argv) {
    printf("softbody testbed v0.0.1\n");

    app_main_loop();

    return 0;
}
