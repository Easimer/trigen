// === Copyright (c) 2020-2021 easimer.net. All rights reserved. ===
//
// Purpose: precompiled header
//

#pragma once

#include <cassert>
#include <cstdio>
#include <cstring>
#include <cerrno>
#include <cstdlib>
